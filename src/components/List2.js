import React from 'react';
import PropTypes from 'prop-types';

function List2({result}){
    

    if(!result || result.length === 0){
        return 'No results found';
    }
    console.log(result);
    return (
      <div>
              {result.map((item) =>{
          return(<div key={item.id}>{item.name} - {item.owner.login}</div>);
          
        })}
      </div>
    );
}

export default List2;

List2.propTypes = {
    result: PropTypes.array
}