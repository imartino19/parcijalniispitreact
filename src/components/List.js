import React from 'react';
import PropTypes  from 'prop-types';

function List({result}){

console.log(result);

    if(!result || result.length === 0){
        return 'No results found';
    }
    return (
      <div>
        <img src={result.avatar_url} alt={result.name}/>
        <br/>
        Name:  {result.name} 
        <br/>
        Location: {result.location} 
        <br/>
        Bio: {result.bio}
      </div>
    );
}

export default List;

List.propTypes = {
    result: PropTypes.object
}