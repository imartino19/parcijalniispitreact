import React from 'react';
import {apiUrl, otherApi, rest} from '../Common';
import PropTypes from 'prop-types';

class Search extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            query: ''
        }
    }

    handleChange = (e) =>{
        this.setState({query: e.target.value})
    }

    handleClick = (e) =>{
        e.preventDefault();
        if(!this.state.query || this.state.query.length === 0){
            return;
        } 

        // fetch(apiUrl + this.state.query)
        // .then(response => response.json()) 
        // .then(dataObject => {
        //     this.props.onResult(dataObject)
        //     this.setState({query: ''})
        // });

        Promise.all([
            fetch(apiUrl + this.state.query).then(response => response.json()) ,
            fetch(otherApi + this.state.query + rest).then(response => response.json()) ,
        ]).then(([result1, result2]) =>{
            this.props.onResult([result1, result2]);
            // console.log(result1);
            // console.log(result2);
            this.setState({query: ''})
        })
    }


    render(){
        return(
            <form onSubmit={this.handleClick} style={{margin: 30}}>
                <input type="text" value={this.state.query} onChange={this.handleChange}/>
               <input type="submit" value="Search"/>
            </form>
        );
    }
}

export default Search;

Search.propTypes = {
    onResult: PropTypes.func
}