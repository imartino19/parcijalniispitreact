import React from 'react';
import './App.css';
import Search from './components/Search';
import List from './components/List';
import List2 from './components/List2';


class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      results: [],
      otherResults: []
    }
  }

  
  handleResult = (result) =>{
    console.log(result[0]);
    console.log(result[1]);
    this.setState({results: result[0], otherResults:result[1]});
  }


  render(){
    return (
      <div className="App">
        <Search onResult={this.handleResult}/>
        <List result={this.state.results} />
        <hr/>
        <List2 result={this.state.otherResults}/>
      </div>
    );
  }
 
}

export default App;
